import { LitElement, html } from 'lit-element';
import "../reserve-ficha-listado/reserve-ficha-listado.js"

class UserFichaListado extends LitElement {

    static get properties() {
        return {
            id: { type: String },
            name: { type: String },
            departamento: { type: String },
            cargo: { type: String },
            imagen: { type: String },
            reserves: {type: Array}
        };
    }

    constructor() {
        super();
        this.reserves= []
        if (this.imagen === undefined) {
            this.imagen="https://image.flaticon.com/icons/png/512/16/16363.png"
        }
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="card h-100">
                <img class="rounded mx-auto d-block" src="${this.imagen}" height="150" width="150" alt="${this.name}">
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.cargo} - ${this.departamento}</p>
                </div>
                <div class="card-footer">
                    <button @click="${this.listReserves}" class="btn btn-success col-5"><strong>Ver reservas</strong></button>
                </div>
                <div class="card-footer">
                    <button @click="${this.editUser}" class="btn btn-primary col-5"><strong>Editar</strong></button>
                    <button @click="${this.deleteUser}" class="btn btn-danger col-5"><strong>Eliminar</strong></button>
                </div>
            </div>
        `;
    }

    listReserves() {
        console.log("listReserves en persona-ficha-listado");
        console.log("Se ha pedido reservas de la persona " + this.id)

        this.dispatchEvent(
            new CustomEvent(
                "user-reserves",
                {
                    "detail": {
                        "id": this.id
                    }
                }

            )
        );
    }

    deleteUser() {
        console.log("deleteUser");
        console.log("Borrando un usuario");
        
        let xhr = new XMLHttpRequest();

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
            }

            this.dispatchEvent(new CustomEvent("user-deleted", {}))
        };

        xhr.open("DELETE", "http://localhost:8088/equipo8/v1/users/" + this.id);
        xhr.send();
        console.log("Fin de deleteUser");
    }

    editUser(e) {
        console.log("moreInfo en persona-ficha-listado");
        console.log("Se ha pedido más información de la persona " + this.id)

        this.dispatchEvent(
            new CustomEvent(
                "user-edited",
                {
                    "detail": {
                        "id": this.id
                    }
                }

            )
        );

    }

}

customElements.define('user-ficha-listado', UserFichaListado)