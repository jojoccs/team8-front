import { LitElement, html } from 'lit-element';

class ReserveFichaListado extends LitElement {

    static get properties() {
        return {
            id: { type: String },
            idPuesto: { type: String },
            idUsers : { type: String },
            dateAlta: { type: String },
            dateBaja: { type: String },
            users: { type: Array },
            products: { type: Array }
        };
    }

    constructor() {
        super();
    }

    render() {
       let product=this.products.filter(product => product.id === this.idPuesto)[0]
       let user=this.users.filter(user => user.id === this.idUsers)[0]
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="card h-100">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Puesto </strong>${product.edificio} - ${product.planta} - ${product.puesto}</li>
                        <li class="list-group-item"><strong>Usuario </strong>${user.name}</li>
                        <li class="list-group-item"><strong>Desde </strong>${new Date(this.dateAlta).toDateString()}</li>
                        <li class="list-group-item"><strong>Hasta </strong>${new Date(this.dateBaja).toDateString()}</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.editReserve}" class="btn btn-primary col-5"><strong>Editar</strong></button>
                    <button @click="${this.deleteReserve}" class="btn btn-danger col-5"><strong>Eliminar</strong></button>
                </div>
            </div>
        `;
    }


    deleteReserve() {
        console.log("deleteReserve");
        console.log("Borrando una reserva");
        
        let xhr = new XMLHttpRequest();

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
            }
            this.dispatchEvent(new CustomEvent("reserve-deleted", {}))
        };

        xhr.open("DELETE", "http://localhost:8088/equipo8/v1/reservas/" + this.id);
        xhr.send();
        console.log("Fin de deleteReserve");
    }

    editReserve(e) {
        console.log("Inicio editReserve en reserve-ficha-listado");
        console.log("Se ha pedido más información de la resereva " + this.reserve)

        this.dispatchEvent(
            new CustomEvent(
                "reserve-edited",
                {
                    "detail": {
                        "id": this.id
                    }
                }

            )
        );

    }

}

customElements.define('reserve-ficha-listado', ReserveFichaListado)