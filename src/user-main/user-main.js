import { LitElement, html } from 'lit-element';
import "../user-ficha-listado/user-ficha-listado.js"
import "../user-form/user-form.js"
import "../reserve-main/reserve-main.js"

class UserMain extends LitElement {

    static get properties() {
        return {
            users: { type: Array },
            showUserForm: { type: Boolean },
            showUserReserve: { type: Boolean },
            filtered: { type: Boolean }
        };
    }

    constructor() {
        super();
        this.users = []
        this.showUserForm=false
        this.showUserReserve=false
        this.filtered=false
        this.getUsers()
    }

    getUsers(search) {
        console.log("getUsers");
        console.log("Obteniendo datos de los usuarios");
        
        let xhr = new XMLHttpRequest();
        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");
                this.filtered=false
                let APIResponse = JSON.parse(xhr.responseText); 
                this.users = APIResponse;
                if (search !== undefined && search !== ""){
                    this.users = this.users.filter(
                        user => user.name.toLowerCase().startsWith(search.toLowerCase())
                    )
                    this.filtered=true
                }
                
            }
        };

        xhr.open("GET", "http://localhost:8088/equipo8/v1/users");
        xhr.send();
        console.log("Fin de getUsers");


    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5" id="usersList">
                ${this.filtered === true ?html`
                    <button @click="${this.usersReload}" class="btn btn-info col-1"><strong>Volver</strong></button>
                `:""}
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.users.map(
                        user => html`<user-ficha-listado 
                                    id="${user.id}" 
                                    name="${user.name}"
                                    departamento="${user.departamento}" 
                                    cargo="${user.cargo}"
                                    @user-deleted="${this.usersReload}" 
                                    @user-edited="${this.editUser}" 
                                    @user-reserves="${this.userReserves}"
                                >
                                </user-ficha-listado>`
                    )}
                </div> 
            </div>
            <div class="row">
                <user-form id="userForm" class="d-none border rounded"
                    @user-form-return="${this.usersReload}">
                </user-form>                
            </div>      
            <div class="row">
                <reserve-main id="userReserveList" class="d-none col-10"
                    @user-reserves="${this.userReserves}"
                    @reserve-list-return="${this.usersReload}"></reserve-main> 
                <user-form id="userForm" class="d-none border rounded"
                    @user-form-return="${this.usersReload}">
                </user-form>                
            </div> 
        `;
    }

    updated(changedProperties){
        console.log("updated")

        if (changedProperties.has("showUserForm")){
            console.log("Ha cambiado el valor de la propiedad showUserForm en user-main")

            if (this.showUserForm === true) {
                this.showUserFormData()
            } else {
                this.showUserList()
            }
        } else if (changedProperties.has("showUserReserve")){
            console.log("Ha cambiado el valor de la propiedad showUserReserve en user-main")

             if (this.showUserReserve === true) {
                this.showUserReserves()
            } else {
                this.showUserList()
            }
        }

    }

    showUserFormData(){
        console.log("showUserFormData")
        console.log("Mostrando formulario de usuario")

        this.shadowRoot.getElementById("userForm").classList.remove("d-none")
        this.shadowRoot.getElementById("usersList").classList.add("d-none")
        this.shadowRoot.getElementById("userReserveList").classList.add("d-none")
    }

    showUserList(){
        console.log("showUserList")
        console.log("Mostrando listado de usuarios")

        
        this.shadowRoot.getElementById("userForm").classList.add("d-none")
        this.shadowRoot.getElementById("usersList").classList.remove("d-none")
        this.shadowRoot.getElementById("userReserveList").classList.add("d-none")
    }

    showUserReserves(){
        console.log("showUserReserves")
        console.log("Mostrando listado de reservas de usuario")

        
        this.shadowRoot.getElementById("userForm").classList.add("d-none")
        this.shadowRoot.getElementById("usersList").classList.add("d-none")
        this.shadowRoot.getElementById("userReserveList").classList.remove("d-none")
    }

    usersReload() {
        console.log("usersReload");
        console.log("Recargando usuarios");
        this.showUserForm = false;
        this.showUserReserve = false;
        this.getUsers()
    }

    editUser(e) {
        console.log("editUser en user-main")

        let chosenUser = this.users.filter(
            user => user.id === e.detail.id
        )
        let userToShow = {}

        userToShow = chosenUser[0]

        console.log(userToShow)

        this.shadowRoot.getElementById("userForm").user = userToShow;
        this.shadowRoot.getElementById("userForm").editingUser = true

        this.showUserForm = true;
        this.showUserReserve = false;
    }

    userReserves(e) {
        console.log("userReserves en user-main")

        let chosenUser = this.users.filter(
            user => user.id === e.detail.id
        )
        let userToShow = {}

        userToShow = chosenUser[0]

        console.log(userToShow)

        this.shadowRoot.getElementById("userReserveList").user = userToShow;
        //this.shadowRoot.getElementById("userReserveList").filterByUser = true

        this.showUserReserve = true;
    }

}

customElements.define('user-main', UserMain)