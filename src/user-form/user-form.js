import { LitElement, html } from 'lit-element';

class UserForm extends LitElement {

    static get properties() {
        return {
            user: { type: Object },
            editingUser: { type: Boolean }
        };
    }

    constructor() {
        super();
        this.resetFormData();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5">
                <form>
                    <div class="form-group">
                        <label>Avatar</label>
                        <input type="text" @input="${this.updateAvatar}" .value="${this.user.imagen}" class="form-control"
                            placeholder="Avatar" />
                    </div>    
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input type="text" @input="${this.updateNombre}" .value="${this.user.name}" class="form-control"
                            placeholder="Nombre completo" />
                    </div>     
                    <div class="form-group">
                        <label>Departamento</label>
                        <input type="text" @input="${this.updateDepartamento}" .value="${this.user.departamento}" class="form-control"
                        placeholder="Departamento" />
                    </div>
                    <div class="form-group">
                        <label>Cargo</label>
                        <input type="text" @input="${this.updateCargo}" .value="${this.user.cargo}" class="form-control"
                            placeholder="Cargo" />
                    </div>
            
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeUser}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent(new CustomEvent("user-form-return", {}))
    }

    updateAvatar(e) {
        console.log("updateAvatar");
        console.log("Actualizando la propiedad avatar con el valor " + e.target.value);
        this.user.avatar = e.target.value;
    }

    updateNombre(e) {
        console.log("updateNombre");
        console.log("Actualizando la propiedad nombre con el valor " + e.target.value);
        this.user.name = e.target.value;
    }

    updateDepartamento(e) {
        console.log("updateDepartamento");
        console.log("Actualizando la propiedad departamento con el valor " + e.target.value);
        this.user.departamento = e.target.value;
    }

    updateCargo(e) {
        console.log("updateCargo");
        console.log("Actualizando la propiedad cargo con el valor " + e.target.value);
        this.user.cargo = e.target.value;
    }

    storeUser(e) {
        console.log("storeUser");
        e.preventDefault();

        console.log(this.user);

        if (this.editingUser === true) {
            console.log("Se va a actualizar un usuario")
            this.updateUser();
        } else {
            console.log("Se va a almacenar un usuario nuevo");
            this.addUser();
        }

        console.log("Fin de storeUser");
    }


    updateUser() {
        console.log("updateUser")

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Usuario actualizado correctamente");
            }
            this.resetFormData();
            this.dispatchEvent(new CustomEvent("user-form-return", {}))
        };

        xhr.open("PUT", "http://localhost:8088/equipo8/v1/users/" + this.user.id);
        xhr.setRequestHeader("Content-type", "application/json")
        xhr.send(JSON.stringify(this.user));

        console.log("Fin updateUser")

    }

    addUser() {
        console.log("addUser")

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Usuario creado correctamente");
            }
            this.resetFormData();
            this.dispatchEvent(new CustomEvent("user-form-return", {}))
        };

        xhr.open("POST", "http://localhost:8088/equipo8/v1/users");
        xhr.setRequestHeader("Content-type", "application/json")
        xhr.send(JSON.stringify(this.user));

        console.log("Fin addUser")
    }

    resetFormData() {
        console.log("resetFormData");
        this.user = {};
        this.user.departamento = "";
        this.user.cargo = "";
        this.user.name = "";
        this.user.imagen = "https://image.flaticon.com/icons/png/512/16/16363.png";
        this.editingUser = false;
    }


}

customElements.define('user-form', UserForm)