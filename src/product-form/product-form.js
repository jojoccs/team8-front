import { LitElement, html } from 'lit-element';

class ProductForm extends LitElement{

    static get properties(){
        return{
            product: {type: Object},
            editingProduct: {type:Boolean}
        };
    }

    constructor(){
        super();
        this.resetFormData();
    }
    
    // .value hace referencia al atributo no a la propiedad, con .value hace referencia al primer valor que tiene al entrar, es propio de javascript
    // ?disabled="${this.editingPerson}" la ? hace relación a que es un boolean, es propio de LiteElement
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5">
                <form> 
                    <div class="form-group">
                        <label>Edificio</label>
                        <input type="text" @input="${this.updateEdificio}" .value="${this.product.edificio}" class="form-control"
                            placeholder="Edificio" />
                    </div>    
                    <div class="form-group">
                        <label>Planta</label>
                        <input type="text" @input="${this.updatePlanta}" .value="${this.product.planta}" class="form-control"
                            placeholder="Planta" />
                    </div>    
                    <div class="form-group">
                        <label>Puesto</label>
                        <input type="text" @input="${this.updatePuesto}" .value="${this.product.puesto}" class="form-control"
                            placeholder="Puesto" />
                    </div>   
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeProduct}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log(" Inicio de goBack");	  
        e.preventDefault();	
        //Para limpiar cuando se da al boton de atrás
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("product-form-return",{}));	
    }

    updateAvatar(e) {
        console.log("Inicio de updateAvatar");
        console.log("Actualizando la propiedad avatar con el valor " + e.target.value);
        this.user.avatar = e.target.value;
    }

    updatePuesto(e) {
        console.log("updatePuesto");
        console.log("Actualizando la propiedad Puesto con el valor " + e.target.value);
        this.product.puesto = e.target.value;
    }
    
    updateEdificio(e) {
        console.log("updateEdificio");
        console.log("Actualizando la propiedad Edificio con el valor " + e.target.value);
        this.product.edificio = e.target.value;
    }
    
    updatePlanta(e) {
        console.log("updatePlanta");
        console.log("Actualizando la propiedad Planta con el valor " + e.target.value);
        this.product.planta = e.target.value;
    }

    storeProduct(e) {
        console.log("Inicio de storeProduct");
        e.preventDefault();
                  
        console.log(this.product);
        
        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar un producto: " + e.detail.product)
            this.updateProduct();

        } else{
            console.log("Se va a almacenar un producto nuevo");
            this.addProduct();
        }
        this.dispatchEvent(new CustomEvent("product-form-return", {}))
        console.log("Fin de storeProduct");
    }


    updateProduct() {
        console.log("Inicio updateProduct");

        let xhr = new XMLHttpRequest(); //let crea variable sólo en el bloque (funcion, bucle, ...) donde se declara

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición update (PUT) completada correctamente");              
            }
            this.dispatchEvent(new CustomEvent("product-form-return", {}))
        };

        xhr.open("PUT", "http://localhost:8088/equipo8/v1/puestos/" + this.id);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.send(this.product);
  
        console.log("Fin updateProduct");

    }

    addProduct() {
        console.log("Inicio addProduct")
        

        let xhr = new XMLHttpRequest(); //let crea variable sólo en el bloque (funcion, bucle, ...) donde se declara
        
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición alta (POST) completada correctamente");               
            } else {
                console.log(xhr.status);
            }
            this.dispatchEvent(new CustomEvent("product-form-return", {}))
        };

        xhr.open("POST", "http://localhost:8088/equipo8/v1/puestos/");
        xhr.setRequestHeader("Content-type", "application/json")
        xhr.send(JSON.stringify(this.product));
          
        console.log("Fin addProduct")
    }

    //Para limpiar los valores de los campos, lo utilizaremos en dos casos:
    //1 al dar al botón de Más 
    //2 al dar botón de Info luego Atrás y luego a Más
    resetFormData() {
        console.log(" Inicio de resetFormData");
        this.product = {};
        this.product.puesto = "";
        this.product.edificio = "";
        this.product.planta ="";
    }

    
}

customElements.define('product-form', ProductForm)