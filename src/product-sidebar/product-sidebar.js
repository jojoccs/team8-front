import { LitElement, html } from 'lit-element';

class ProductSidebar extends LitElement{

    static get properties(){
        return{
            product: {type: Object}
        };
    }

    constructor(){
        super();
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">        
            <aside>
                <section>
                    <div class="mt-5">
                        <button @click="${this.newProduct}" class="w-100 btn btn-success" style="font-size: 15"><strong>Agregar Puesto</strong></button>
                    </div>
                </section>
            </aside>        
        `;
    }

    newProduct(e) {
        console.log("newProduct en product-sidebar");
        console.log("Se va a crear un nuevo producto");
      
        this.dispatchEvent(new CustomEvent("new-product", {})); 
    }

}

customElements.define('product-sidebar', ProductSidebar)