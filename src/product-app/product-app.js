import { LitElement, html } from 'lit-element';
import '../product-main/product-main.js';
import '../product-sidebar/product-sidebar.js'

class ProductsApp extends LitElement {

    constructor(){
        super();     
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="row">
                <product-sidebar @new-product="${this.newProduct}" class="col-2"></product-sidebar>    
                <product-main class="col-10"></product-main>
            </div>
        `;
    }

    newProduct(e){
        console.log("newProduct en product-app")
        console.log(e);

        this.shadowRoot.querySelector("product-main").showProductForm = true;
    }

    
    refresh(e){
        console.log("refresh en product-app");
        console.log(e);

        this.shadowRoot.querySelector("product-main").getProducts()
    }

}

customElements.define('product-app', ProductsApp)