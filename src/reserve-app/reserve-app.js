import { LitElement, html } from 'lit-element';
import '../reserve-main/reserve-main.js';
import '../reserve-sidebar/reserve-sidebar.js';

class ReserveApp extends LitElement {

    constructor(){
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="row">
                <reserve-sidebar @new-reserve="${this.newReserve}" class="col-2"></reserve-sidebar>
                <reserve-main class="col-10"></reserve-main>
            </div>
        `;
    }

    
    newReserve(e){
        console.log("newReserve en reserve-app");
        console.log(e);

        this.shadowRoot.querySelector("reserve-main").showReserveForm = true;
    }

    refresh(e){
        console.log("refresh en reserve-app");
        console.log(e);

        this.shadowRoot.querySelector("reserve-main").getProducts()
        this.shadowRoot.querySelector("reserve-main").getUsers()
        this.shadowRoot.querySelector("reserve-main").getReserves()
    }

}

customElements.define('reserve-app', ReserveApp)