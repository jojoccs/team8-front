import { LitElement, html } from 'lit-element';
import '../product-ficha-listado/product-ficha-listado.js'
import '../product-form/product-form.js'

class ProductMain extends LitElement {

    static get properties() {
        return {
            products: { type: Array },
            showProductForm: {type: Boolean}
        };
    }

    constructor() {
        super();
        this.products = [];
        this.showProductForm = false;
        this.getProducts();
    }

    getProducts() {
        console.log("Inicio de getProducts");
        console.log("Obteniendo datos de los puestos");
        
        let xhr = new XMLHttpRequest(); //let crea variable sólo en el bloque (funcion, bucle, ...) donde se declara

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("GET Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText); 
                this.products= APIResponse;                
            }
        };

        xhr.open("GET", "http://localhost:8088/equipo8/v1/puestos/");
        xhr.send();
        console.log("Fin de getProducts");


    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5" id="productList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.products.map(
                        product => html`<product-ficha-listado 
                                    id="${product.id}" 
                                    puesto="${product.puesto}" 
                                    edificio="${product.edificio}" 
                                    planta="${product.planta}"
                                    @product-deleted="${this.productsReload}" 
                                    @product-edited="${this.editProduct}"
                                >
                                </product-ficha-listado>`
                    )}
                </div> 
            </div>
            <div class="row">
                <product-form id="productForm" class="d-none border rounded"
                    @product-form-return="${this.productsReload}">
                </product-form>                
            </div>               
        `;
    }

    updated(changedProperties){
        console.log("Inicio updated en producto-main")

        if (changedProperties.has("showProductForm")){
            console.log("Ha cambiado el valor de la propiedad showProductForm en product-main a " + this.showProductForm)

            if (this.showProductForm === true) {
                this.showProductFormData()
            } else {
                this.showProductList()
            }
        }
    }

    showProductFormData() {
        console.log("Inicio showProductFormData");
        console.log("Mostrando formulario de producto");
        
        this.shadowRoot.getElementById("productForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("productList").classList.add("d-none");	
    } 	 

    showProductList() {
        console.log("Inicio showPersonList");
        console.log("Mostrando listado de productos");
        this.shadowRoot.getElementById("productForm").classList.add("d-none");	
        this.shadowRoot.getElementById("productList").classList.remove("d-none");
    }  

    ProductFormReturn() {
        console.log("Iniico de productFormReturn");
        console.log("Se ha cerrado el formulario del producto");
        this.showProductForm = false;
        this.getProducts()
    }

    productsReload() {
        console.log("productsReload");
        console.log("Recargando productos");
        this.showProductForm = false;
        this.getProducts()
    }

    editProduct(e) {
        console.log("Inicio editProduct en product-main")
        
        let chosenProduct = this.products.filter(
            product => product.id === e.detail.id
        )
        let productToShow = {}
        
        productToShow = chosenProduct[0]

        console.log(productToShow)

        this.shadowRoot.getElementById("productForm").product = productToShow;
        this.shadowRoot.getElementById("productForm").editingProduct = true

        console.log("Antes de modificar showProductForm")
        this.showProductForm = true;
 
    }

}

customElements.define('product-main', ProductMain)