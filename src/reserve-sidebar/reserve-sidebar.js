import { LitElement, html } from 'lit-element';

class ReserveSidebar extends LitElement{

    constructor(){
        super();
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">        
            <aside>
                <section>
                    <div class="mt-5">
                        <button @click="${this.newReserve}" class="w-100 btn btn-success" style="font-size: 15"><strong>Agregar Reserva</strong></button>
                    </div>
                </section>
            </aside>        
        `;
    }

    newReserve(e) {
        console.log("newReserve en reserve-sidebar");
        console.log("Se va a crear una nueva reserva");
      
        this.dispatchEvent(new CustomEvent("new-reserve", {})); 
    }

}

customElements.define('reserve-sidebar', ReserveSidebar)