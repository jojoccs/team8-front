import { LitElement, html } from 'lit-element';
import "../reserve-ficha-listado/reserve-ficha-listado.js"
import "../reserve-form/reserve-form.js"

class ReserveMain extends LitElement {

    static get properties() {
        return {
            reserves: { type: Array },
            showReserveForm: { type: Boolean },
            users: { type: Array },
            products: { type: Array },
            user : { type: Object }
        };
    }

    constructor() {
        super();
        this.reserves = []
        this.showReserveForm= false
        
        this.getProducts()
        this.getUsers()
        this.getReserves()
    }

    getReserves() {
        console.log("getReserves");
        console.log("Obteniendo datos de las reservas");
        
        let xhr = new XMLHttpRequest();
        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText); 
                if (this.user !== undefined){
                    this.reserves = APIResponse.filter(
                        reserve => reserve.idUsers === this.user.id
                    );
                } else {
                    this.reserves = APIResponse;
                }
            }
        };

        xhr.open("GET", "http://localhost:8088/equipo8/v1/reservas");
        xhr.send();
        console.log("Fin de getReserves");


    }
    getProducts() {
        console.log("getProducts");
        console.log("Obteniendo datos de las reservas");
        
        let xhr = new XMLHttpRequest();
        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText); 
                this.products = APIResponse;
            }
        };

        xhr.open("GET", "http://localhost:8088/equipo8/v1/puestos");
        xhr.send();
        console.log("Fin de getProducts");
    }

    getUsers() {
        console.log("getUsers");
        console.log("Obteniendo datos de las reservas");
        
        let xhr = new XMLHttpRequest();
        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correctamente");

                let APIResponse = JSON.parse(xhr.responseText); 
                this.users = APIResponse;
            }
        };

        xhr.open("GET", "http://localhost:8088/equipo8/v1/users");
        xhr.send();
        console.log("Fin de getUsers");
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5" id="reservesList">
                ${this.user !== undefined ?html`
                    <button @click="${this.goBack}" class="btn btn-info col-1"><strong>Volver</strong></button>
                    <button @click="${this.newReserve}" class="btn btn-success col-1"><strong>Agregar</strong></button>
                `:""}
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.reserves.map(
                        reserve => html`<reserve-ficha-listado 
                                    id="${reserve.id}" 
                                    idUsers="${reserve.idUsers}"
                                    idPuesto="${reserve.idPuesto}" 
                                    dateAlta="${reserve.dateAlta}"
                                    dateBaja="${reserve.dateBaja}"
                                    product="${this.products.filter(product => product.id === reserve.idPuesto)}"
                                    user="${this.users.filter(user => user.id === reserve.idUsers)}"
                                    .products="${this.products}"
                                    .users="${this.users}"
                                    @reserve-deleted="${this.reservesReload}" 
                                    @reserve-edited="${this.editReserve}" 
                                >
                                </reserve-ficha-listado>`
                    )}
                </div> 
            </div>
            <div class="row">
                <reserve-form id="reserveForm" class="d-none border rounded"
                    @reserve-form-return="${this.reservesReload}"
                    .selectedUser=${this.user}
                    .users="${this.users}"
                    .products="${this.products}">
                </reserve-form>                
            </div>         
        `;
    }

    newReserve(e) {
        console.log("newReserve en reserve-main");
        console.log("Se va a crear una nueva reserva");
      
        this.showReserveForm = true;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.dispatchEvent(new CustomEvent("reserve-list-return", {}))
    }

    updated(changedProperties){
        console.log("updated")
        console.log(changedProperties)
        if (changedProperties.has("showReserveForm")){
            console.log("Ha cambiado el valor de la propiedad showReserveForm en reserve-main")

            if (this.showReserveForm === true) {
                this.showReserveFormData()
            } else {
                this.showReserveList()
            }
        }
        if (changedProperties.has("user")){
            this.reservesReload()
        }
    }

    showReserveFormData(){
        console.log("showReserveFormData")
        console.log("Mostrando formulario de reservas")

        this.shadowRoot.getElementById("reserveForm").classList.remove("d-none")
        this.shadowRoot.getElementById("reservesList").classList.add("d-none")
    }

    showReserveList(){
        console.log("showReserveList")
        console.log("Mostrando listado de reservas")

        
        this.shadowRoot.getElementById("reserveForm").classList.add("d-none")
        this.shadowRoot.getElementById("reservesList").classList.remove("d-none")
    }

    reservesReload() {
        console.log("reservesReload");
        console.log("Recargando reservas");
        this.showReserveForm = false;
        this.getUsers();
        this.getProducts();
        this.getReserves()
    }

    editReserve(e) {
        console.log("editReserve en reserve-main")

        let chosenReserve = this.reserves.filter(
            reserve => reserve.id === e.detail.id
        )
        let reserveToShow = {}

        reserveToShow = chosenReserve[0]

        console.log(reserveToShow)

        this.shadowRoot.getElementById("reserveForm").reserve = reserveToShow;
        this.shadowRoot.getElementById("reserveForm").editingReserve = true

        this.showReserveForm = true;
    }

}

customElements.define('reserve-main', ReserveMain)