import { LitElement, html } from 'lit-element';

class ProductsFichaListado extends LitElement {

    static get properties() {
        return {
            id: { type: String },
            puesto: { type: String },
            edificio: { type: String },
            planta: { type: String },
            imagen: { type: String }
        };
    }

    constructor() {
        super();
        if (this.imagen === undefined) {
            this.imagen="https://image.flaticon.com/icons/png/128/2942/2942841.png"
        }
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="card h-100">
                <img class="rounded mx-auto d-block" src="${this.imagen}" height="150" width="150" alt="${this.puesto}">
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item"><strong>Edificio </strong>${this.edificio}</li>
                        <li class="list-group-item"><strong>Planta </strong>${this.planta}</li>
                        <li class="list-group-item"><strong>Puesto </strong>${this.puesto}</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.editProduct}" class="btn btn-primary col-5"><strong>Editar</strong></button>
                    <button @click="${this.deleteProduct}" class="btn btn-danger col-5"><strong>Eliminar</strong></button>
                </div>
            </div>
        `;
    }

    deleteProduct() {
        console.log("deleteProduct");
        console.log("Borrando un producto ");
        
        let xhr = new XMLHttpRequest(); //let crea variable sólo en el bloque (funcion, bucle, ...) donde se declara

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("DELETE Petición completada correctamente");
            }

            this.dispatchEvent(new CustomEvent("product-deleted", {}))
        };

        xhr.open("DELETE", "http://localhost:8088/equipo8/v1/puestos/" + this.id);
        xhr.send();
        console.log("Fin de deleteProduct");


    }

    editProduct(e) {
        console.log("Inicio editProduct en product-ficha-listado");
        console.log("Se ha pedido más información del producto " + this.puesto)

        this.dispatchEvent(
            new CustomEvent(
                "product-edited",
                {
                    "detail": {
                        "id": this.id
                    }
                }

            )
        );

    }

}

customElements.define('product-ficha-listado', ProductsFichaListado)