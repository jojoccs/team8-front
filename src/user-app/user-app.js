import { LitElement, html } from 'lit-element';
import '../user-main/user-main.js';
import '../user-sidebar/user-sidebar.js';

class UserApp extends LitElement {

    constructor(){
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="row">
                <user-sidebar @new-user="${this.newUser}" @search="${this.refresh}" class="col-2"></user-sidebar>
                <user-main class="col-10"></user-main>
            </div>
        `;
    }

    
    newUser(e){
        console.log("newUser en user-app");
        console.log(e);

        this.shadowRoot.querySelector("user-main").showUserForm = true;
    }

    refresh(e){
        console.log("refres en user-app");
        console.log(e);
        if (e !== undefined){
            this.shadowRoot.querySelector("user-main").getUsers(e.detail.search)
        }else {
            this.shadowRoot.querySelector("user-main").getUsers()
        }
    }

}

customElements.define('user-app', UserApp)