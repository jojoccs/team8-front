import { LitElement, html } from 'lit-element';

class ReserveForm extends LitElement {

    static get properties() {
        return {
            reserve: { type: Object },
            editingReserve: { type: Boolean },
            users : { type: Array},
            products : { type: Array },
            selectedUser : { type: Object }
        };
    }

    constructor() {
        super();
        this.reserve = {}
        this.resetFormData();
        this.users = []
        this.products = []
        this.selectedUser = {}
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="mt-5">
                <form>
                    <div class="form-group">
                        <label>Puesto</label>
                        <select class="form-control" @change="${this.updatePuesto}" .value="${this.reserve.idPuesto}">
                            <option disabled selected value=""> -- Selecciona un puesto -- </option>
                            ${this.products.map(option => html`
                                <option value="${option.id}" ?selected=${this.selected === option.id}>${option.edificio} - ${option.planta} - ${option.puesto}</option>
                            `)}
                        </select>
                    </div>    
                    <div class="form-group">
                        <label>Usuario</label>
                            ${this.selectedUser === undefined ?
                                html`
                                <select class="form-control" @change="${this.updateUser}" .value="${this.reserve.idUsers}">
                                    <option selected value=""> -- Selecciona un usuario -- </option>
                                    ${this.users.map(option => html`
                                        <option value="${option.id}">${option.name}</option>
                                    `)}
                                </select>
                            `:
                                html`
                                <select class="form-control disabled" @change="${this.updateUser}" .value="${this.reserve.idUsers}">
                                    <option value="${this.selectedUser.id}" ?selected=${true}>${this.selectedUser.name}</option>
                                </select>
                            `
                        }
                    </div>     
                    <div class="form-group">
                        <label>Fecha desde</label>
                        <input type="text" @input="${this.updateFechaDesde}" .value="${new Date(this.reserve.dateAlta).toLocaleDateString()}" class="form-control"
                            placeholder="Fecha desde" />
                        <!--input type="date" @input="${this.updateFechaDesde}" value="${this.reserve.dateAlta}" class="form-control"
                            placeholder="Fecha desde" /-->
                    </div>
                    <div class="form-group">

                        <label>Fecha hasta</label>
                        <!--input type="date" @input="${this.updateFechaHasta}" value="${this.reserve.dateBaja}" class="form-control"
                            placeholder="Fecha hasta" /-->
                        <input type="text" @input="${this.updateFechaHasta}" .value="${new Date(this.reserve.dateBaja).toLocaleDateString()}" class="form-control"
                            placeholder="Fecha hasta" />
                    </div>
            
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeReserve}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    goBack(e) {
        console.log("goBack");
        e.preventDefault();

        this.resetFormData();
        this.dispatchEvent(new CustomEvent("reserve-form-return", {}))
    }

    updateUser(e) {
        console.log("updateAvatar");
        console.log("Actualizando la propiedad avatar con el valor " + e.target.value);
        this.reserve.idUsers = e.target.value;
    }

    updatePuesto(e) {
        console.log("updateNombre");
        console.log("Actualizando la propiedad nombre con el valor " + e.target.value);
        this.reserve.idPuesto = e.target.value;
    }

    updateFechaDesde(e) {
        console.log("updateFechaDesde");
        console.log("Actualizando la propiedad dateAlta con el valor " + e.target.value);
        this.reserve.dateAlta = new Date(e.target.value);
    }

    updateFechaHasta(e) {
        console.log("updateFechaHasta");
        console.log("Actualizando la propiedad dateBaja con el valor " + e.target.value);
        this.reserve.dateBaja = new Date(e.target.value);
    }

    storeReserve(e) {
        console.log("storeReserve");
        e.preventDefault();

        console.log(this.reserve);
        if (this.selectedUser !== undefined){
            this.reserve.idUsers = this.selectedUser.id
        }
        if (this.editingReserve === true) {
            console.log("Se va a actualizar una reserva")
            this.updateReserve();
        } else {
            console.log("Se va a almacenar una reserva nueva");
            this.addReserve();
        }

        console.log("Fin de storeReserve");
    }


    updateReserve() {
        console.log("updateReserve")

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Reserve actualizado correctamente");
            }
            this.resetFormData();
            this.dispatchEvent(new CustomEvent("reserve-form-return", {}))
        };

        xhr.open("PUT", "http://localhost:8088/equipo8/v1/reservas/" + this.reserve.id);
        xhr.setRequestHeader("Content-type", "application/json")
        xhr.send(JSON.stringify(this.reserve));

        console.log("Fin updateReserve")

    }

    addReserve() {
        console.log("addReserve")

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Reserva creada correctamente");
            }
            this.resetFormData();
            this.dispatchEvent(new CustomEvent("reserve-form-return", {}))
        };

        xhr.open("POST", "http://localhost:8088/equipo8/v1/reservas");
        xhr.setRequestHeader("Content-type", "application/json")
        xhr.send(JSON.stringify(this.reserve));

        console.log("Fin addReserve")
    }

    resetFormData() {
        console.log("resetFormData");
        this.reserve = {};
        this.reserve.idUsers = "";
        this.reserve.idPuesto = "";
        this.reserve.dateAlta = new Date()
        this.reserve.dateBaja = new Date()
       // this.shadowRoot.querySelectorAll("input[type=date]").forEach(input => input.value = null) 
        this.shadowRoot.querySelectorAll("select").forEach(select => select.value = "") 
        this.selectedUser = undefined
        this.editingReserve = false;
    }


}

customElements.define('reserve-form', ReserveForm)