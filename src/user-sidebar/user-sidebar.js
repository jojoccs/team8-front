import { LitElement, html } from 'lit-element';

class UserSidebar extends LitElement{

    static get properties(){
        return{
            user: {type: Object}
        };
    }

    constructor(){
        super();
    }
    
    render(){
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">        
            <aside>
                <section>
                    <div class="mt-5">
                        <button @click="${this.newUser}" class="w-100 btn btn-success" style="font-size: 15"><strong>Agregar Usuario</strong></button>
                    </div>
                    <div class="mt-5"></div>
                        <form >
                            <input id="inputSearch" style="width: 60%" type="search" placeholder="Buscar usuario" aria-label="Buscar">
                            <button class="btn btn-dark" @click="${this.search}">Buscar</button>
                        </form>
                    </div>
                </section>
            </aside>        
        `;
    }

    newUser(e) {
        console.log("newUser en user-sidebar");
        console.log("Se va a crear un nuevo usuario");
      
        this.dispatchEvent(new CustomEvent("new-user", {})); 
    }

    search(e) {
        console.log("enter search")
        e.preventDefault()
        this.dispatchEvent(new CustomEvent("search", {
            "detail": {
                "search":  this.shadowRoot.getElementById("inputSearch").value
            }
        })); 
        this.shadowRoot.getElementById("inputSearch").value = ""
    }

}

customElements.define('user-sidebar', UserSidebar)