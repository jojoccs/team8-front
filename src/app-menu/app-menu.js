import { LitElement, html } from 'lit-element';
import "../product-app/product-app.js"
import "../user-app/user-app.js"
import "../reserve-app/reserve-app.js"

class AppMenu extends LitElement {

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
                integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
                    aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <a class="navbar-brand" href="#">Hackathon Team 8</a>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">   
                        <li class="nav-item">
                            <a id="buttonUsers" class="nav-link active" @click="${this.showUser}">Usuarios</a>
                        </li>
                        <li class="nav-item active">
                            <a id="buttonProducts" class="nav-link" @click="${this.showProducts}">Puestos</a>
                        </li>
                        <li class="nav-item">
                            <a id="buttonReserves" class="nav-link" @click="${this.showReserves}">Reservas</a>
                        </li>
                    </ul>
                </div>
                <a class="navbar-brand">Herramienta de gestión de puestos</a>
            </nav>
            <div class="row">
                <user-app class=""></user-app>
                <product-app class="d-none"></product-app>
                <reserve-app class="d-none"></reserve-app>
            </div>

        `;
    }

    showUser(e) {
        e.preventDefault()
        console.log("showUser");

        this.shadowRoot.getElementById("buttonProducts").classList.remove("active")
        this.shadowRoot.getElementById("buttonUsers").classList.add("active")
        this.shadowRoot.getElementById("buttonReserves").classList.remove("active")
        this.shadowRoot.querySelector("user-app").classList.remove("d-none")
        this.shadowRoot.querySelector("product-app").classList.add("d-none")
        this.shadowRoot.querySelector("reserve-app").classList.add("d-none")
        this.shadowRoot.querySelector("user-app").refresh()
    }

    showProducts(e) {
        e.preventDefault()
        console.log("showProducts");

        this.shadowRoot.getElementById("buttonProducts").classList.add("active")
        this.shadowRoot.getElementById("buttonUsers").classList.remove("active")
        this.shadowRoot.getElementById("buttonReserves").classList.remove("active")
        this.shadowRoot.querySelector("user-app").classList.add("d-none")
        this.shadowRoot.querySelector("product-app").classList.remove("d-none")
        this.shadowRoot.querySelector("reserve-app").classList.add("d-none")
        this.shadowRoot.querySelector("product-app").refresh()
    }

    showReserves(e) {
        e.preventDefault()
        console.log("showReserves");

        this.shadowRoot.getElementById("buttonProducts").classList.remove("active")
        this.shadowRoot.getElementById("buttonUsers").classList.remove("active")
        this.shadowRoot.getElementById("buttonReserves").classList.add("active")
        this.shadowRoot.querySelector("user-app").classList.add("d-none")
        this.shadowRoot.querySelector("product-app").classList.add("d-none")
        this.shadowRoot.querySelector("reserve-app").classList.remove("d-none")
        this.shadowRoot.querySelector("reserve-app").refresh()
    }

}

customElements.define('app-menu', AppMenu)